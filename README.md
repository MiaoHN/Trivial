# Trivial

一些琐碎的代码，放到一起方便管理

## 目录

- [json 解析器](trivial/json)
- [线程池](trivial/thread_pool)
- [Coroutine](trivial/coroutine)